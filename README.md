## About this repo

**NOTE**: This code is not optimised nor well tested, use carefully!

This repository includes the class ```Model_builder``` that allows to load a URDF model into RBDL model, with the possibility to enable/disable actuated joints.
It was thought to be used with the robot ```iCubHeidelberg01``` or in general bipedal robots with URDF models and together with the pattern generator module in MUSCOD.
In the current implementation the model of ```iCubGenova01``` is used with only the lower body 15 DOF actuated.

Clone this repository with ```git clone```.

Compile the project in ```icub_walk_pg_ik``` by:
```
cd icub_walk_pg_ik
mkdir build
cd build
ccmake ..
make -j4
```

To run the example walking motion obtained using pattern generator and inverse kinematics, after you build the project, in a terminal, inside the build folder type:
```
./icub_walk ../LongWalk/time_1.5
```
The computation takes some time (like 30 sec/1 min according to the length of your trajectory).
The output files will be saved under ```../LongWalk/time_1.5```.

There is also a ```.lua``` model file that you can visualize in [MeshUp](https://bitbucket.org/MartinFelis/meshup/) Unfortunately there are no meshes, so you will see only the frames moving around.
The generated motion to be used in MeshUp is under ```LongWalk/time_1.5/test_ik_pg_meshup.csv```.

### How to use the code
The code takes as input a file in csv format and produces as outputs two csv files.

The input file is from the pattern generator and should have the following format:
``` time[s] com_x[m] com_y[m] zmp_x[m] zmp_y[m]```

The output file, called ```test_ik_pg.csv```, is the one that should be used on the robot, contains the joint trajectories in degrees.

Take a look at the comments in the code for more details.

### Dependencies
You need to install RBDL from [here](https://bitbucket.org/yue_hu/rbdl).
And enable ```RBDL_BUILD_ADDON_LUAMODEL=ON``` and  ```RBDL_BUILD_ADDON_URDFREADER=ON```.

