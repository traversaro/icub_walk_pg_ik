#include<iostream>
#include<sstream>
#include<fstream>
#include "UtilityFunctions.h"

using namespace std;

void readFromCSV(string filename, vector<RigidBodyDynamics::Math::VectorNd>& mat)
{
  ifstream file(filename.c_str());
  string line;
  int rows;
  int cols;
  rows = mat.size();
  cols = mat[0].size();

  for(int row = 0; row < rows; row++)
  {
      getline(file, line);
      if ( !file.good() )
      break;

      stringstream iss(line);
      for (int col = 0; col < cols; col++)
      {
      string val;
      getline(iss, val, ',');
      if ( !iss.good() )
          break;

      stringstream convertor(val);
      convertor >> mat[row][col];
      }
  }

  file.close();
}

void writeOnCSV(RigidBodyDynamics::Math::VectorNd time, vector<RigidBodyDynamics::Math::VectorNd>& data, string file_name, const char* header)
{
  FILE *fp;
  stringstream output_filename ("");  

  output_filename << file_name;
  
  fp = fopen (output_filename.str().c_str(), "w");

  if (!fp) {
    fprintf (stderr, "Error: Could not open file '%s'!\n", output_filename.str().c_str());
    return;
  }
  
  if(header != "")
    fprintf (fp, "%s\n", header);
  
  for(int i = 0; i < time.size(); i++)
  {
    // time
    fprintf (fp, "%e,\t", time[i]);

    // joint values
    for (int j = 0; j < data[i].size(); j++)
      fprintf (fp, "%e,\t", data[i][j]);
      
    fprintf (fp, "\n");
  }
  
  fclose (fp);
}

void writeOnCSV(vector<RigidBodyDynamics::Math::VectorNd>& data, string file_name)
{
  FILE *fp;
  stringstream output_filename ("");  

  output_filename << file_name;
  
  fp = fopen (output_filename.str().c_str(), "w");

  if (!fp) {
    fprintf (stderr, "Error: Could not open file '%s'!\n", output_filename.str().c_str());
    return;
  }
  
  for(int i = 0; i < data.size(); i++)
  {
    // joint values
    for (int j = 0; j < data[i].size(); j++)
      fprintf (fp, "%e,\t", data[i][j]);
      
    fprintf (fp, "\n");
  }
  
  fclose (fp);
}